

  
    // function getElementY(query) {
    //     return window.pageYOffset + document.querySelector(query).getBoundingClientRect().top
    // }
    
    function doScrolling(element, duration) {
        var startingY = window.pageYOffset
        var elementY =element   //   var elementY = getElementY(element)
        // If element is close to page's bottom then window will scroll only to some position above the element.
        var targetY = document.body.scrollHeight - elementY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elementY
        var diff = targetY - startingY
        // Easing function: easeInOutCubic
        // From: https://gist.github.com/gre/1650294
        var easing = function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 }
        var start
    
        if (!diff) return
    
        // Bootstrap our animation - it will get called right before next frame shall be rendered.
        window.requestAnimationFrame(function step(timestamp) {
        if (!start) start = timestamp
        // Elapsed miliseconds since start of scrolling.
        var time = timestamp - start
            // Get percent of completion in range [0, 1].
        var percent = Math.min(time / duration, 1)
        // Apply the easing.
        // It can cause bad-looking slow frames in browser performance tool, so be careful.
        percent = easing(percent)
    
        window.scrollTo(0, startingY + diff * percent)
    
            // Proceed with animation as long as we wanted it to.
        if (time < duration) {
            window.requestAnimationFrame(step)
        }
        })
    }
    const parentEventListener = () => {
        var eventMethod = window.addEventListener
        ? "addEventListener"
        : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod === "attachEvent"
        ? "onmessage"
        : "message";
        eventer(messageEvent, function (e) {
            
        //Origin allows
        if (!(e.origin === 'https://ww2.sample.ca' 
        || e.origin === 'https://localhost' 
        || e.origin === 'https://sample.ca' 
        || e.origin === 'https://www.sample.ca')) return;

        if(e.data && e.data.newStateData){
            if (e.data.newStateData.name === "rizeParentHeightEvent" ) 
            {
                var mainElement = document.querySelector("#main");
                var awFrameElement = document.querySelector("#sample-productsframe");
                
                if(e.data.newStateData.childHeight && mainElement && awFrameElement){
                    var _t ='height: '+ (e.data.newStateData.childHeight) +'px !important';
                    var _tParent ='height: '+ (e.data.newStateData.childHeight + 100) +'px !important';
                    mainElement.setAttribute('style', _tParent  );
                    awFrameElement.setAttribute('style', _t ); //This element should be created on the wordpress page
                }else if(!awFrameElement || !mainElement){ 
                    console.error("Element missing on parent site >>> #main, #sample-productsframe" );
                }
            }
            else if (e.data.newStateData.name === "spinnerEvent" ) 
            {
                var elem =document.querySelector("#infinite-content-loader");
                if(e.data.newStateData.isVisibled && elem){
                    document.querySelector("#infinite-content-loader").setAttribute('style', 'display:inline !important; top: -10px !important; z-index: 190000 !important; left: 0px; position: fixed !important;' );
                }else if(!e.data.newStateData.isVisibled && elem){
                    document.getElementById('infinite-content-loader').style.display= 'none';
                }else{
                    console.error("Element missing on parent site >>> #infinite-content-loader" );
                }
            }
            else if (e.data.newStateData.name === "scrollInParentEvent" ) 
            {
                if(e.data.newStateData.direction === "top"){
                    var _to = e.data.newStateData.scrollTo - 50;
                    doScrolling(_to, e.data.newStateData.scrollDuration);
                }
            }
            else if (e.data.newStateData.name === "redirectUrlEvent" ) 
            {
                window.location.href = e.data.newStateData.url;
            }
            else if (e.data.newStateData.name === "displayMessageEvent" ) 
            {
                var options = e.data.newStateData.options;
                var type = e.data.newStateData.type;
                var message = e.data.newStateData.XHR.message;
                var title = e.data.newStateData.title;

                toastr.clear();
                if(e.data.newStateData.type === 'error')
                {
                     toastr['error'](message === undefined ? e.data.newStateData.XHR.statusText : message, title, options);
                }else if(title === 'Click to restore your session'){
                    toastOptionsForRestoringSession = {
                        timeOut: 90000,
                        fadeOut: 90000,
                        onclick: requiredSessionRestoreFromParent
                    };
                    toastr[type]( message, title, toastOptionsForRestoringSession);
                }
                else{
                    toastr[type]( message, title);
                }
                document.querySelector("#toast-container").setAttribute('style', 'top: 8% !important; z-index: 190000 !important;position: fixed !important;' );
            }
        }
        });

    };

    function requiredSessionRestoreFromParent(){
        //Send a post message to the child frame
        var iFrame = document.getElementById('sample-productsframe');

        const newStateData = {
            name: 'sessionRestoreFromParentEvent',
          }
          iFrame.contentWindow.postMessage({ newStateData }, '*');
    }


    (function() {
        parentEventListener();
     })();


