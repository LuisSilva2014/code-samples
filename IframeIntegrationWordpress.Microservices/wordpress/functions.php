
<?php
/**
 * Extra files & functions are hooked here.
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package Avada
 * @subpackage Core
 * @since 1.0
 */ 

 ... 
/**
 * Bootstrap the theme.
 *
 * @since 6.0
 */
require get_template_directory() . '/includes/bootstrap.php';

/* Omit closing PHP tag to avoid "Headers already sent" issues. */


//Hide the header and handled the new title
function customs_theme_scripts() {
	

	wp_enqueue_style( 'style-toast-aw', 'https://ww2.sample/microservice1/public/css/toast.min.css' ); //Style of the toaster

	wp_enqueue_script( 'wordpres-toastr-script', 'https://ww2.sample/microservice1/public/js/toastr.js', array('jquery')); //Script to include the toaster 
	
	wp_enqueue_script( 'wordpres-injected-script', 'https://ww2.sample/microservice1/public/js/utilities.wordpress.injected.js', array('jquery'));

} 

add_action( 'wp_enqueue_scripts', 'customs_theme_scripts' );