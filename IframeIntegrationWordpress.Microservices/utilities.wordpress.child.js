
"use strict";

// Create the validation namespace if it does not already exist
(function (utilities, $, undefined) {

    // Create the string namespace
    var _wordpress = {
    };
     // Create the string namespace
    var _child = {
    };
    utilities.wordpress = _wordpress;
    utilities.wordpress.child = _child;

    //For debugging on browser
    utilities.wordpress.child.isDebugging = isDevMode;
 
    utilities.wordpress.child.initResizeObserverOnInnerFrame = function (e) {

        const container = $(document.body)[0];
        var observer;
        let previousElementHeight = 0;
        const defaultSize = 900;
        const minimumSize = 300;
        const margin = 50;

        //Initialize observer object
        const initObserver = () => {
                if (typeof ResizeObserver !== 'undefined') {
                    //Resize observer to get changes of body height
                    observer = new ResizeObserver(entries => {
                        try {
                            const element = entries[0].contentRect;

                            if (element.height > minimumSize) {
                                resizeIframeFromParent(element.height);
                                if (utilities.wordpress.child.isDebugging)
                                    console.log("resizeIframeFromParent with height: " + element.height);
                            }
                        } catch (e) {
                            console.log('There was an exception on resize functionality >>> ', e);
                        }
                    });

                } else {
                    
                    //Mutation observer to get changes of body height when browser does not support ResizeObserver API
                    observer = new MutationObserver((mutationsList) => {
                        try {
                            const hasChangedStyle = mutationsList.some( function (mutation) {
                                return (mutation.type === 'attributes' && mutation.attributeName==='style');  
                            });

                            if (hasChangedStyle) {
                                var height = $(container).height();                           
                                if (height > minimumSize) {
                                    height = height < defaultSize ? defaultSize : height;
                                    if (previousElementHeight !== height && previousElementHeight !== (height + margin)) {

                                        resizeIframeFromParent(height);
                                        if (utilities.wordpress.child.isDebugging)
                                        {
                                            console.log('Client resize. New height >>> ', previousElementHeight );
                                            console.log("resizeIframeFromParent with height: " + height);
                                        } 
                                    }
                                }
                            }
                        } catch (e) {
                            console.log('There was an exception on resize functionality >>> ', e);
                        }                       
                    });               
                }
            }

            //Set container to observer object
            const setObserver = container => {
                try {
                    if (typeof ResizeObserver !== 'undefined') {
                        observer.observe(container);
                    } else {
                        const config = { attributes: true, childList: false, subtree: true };
                        observer.observe(container, config);
                    }                
                } catch (e) {
                    console.log('There was an exception on setObserver function for resize functionality >>> ', e);
                }               
            }
            
            
            //Inittialize observer object
            initObserver();

            //Set observer for body
            setObserver(container);
     };


     utilities.wordpress.child.scrollInParent = function (_element, _scrollDuration, _direction, _scrollTo)  {

        const newStateData = {
            name: 'scrollInParentEvent',
            element: _element,
            scrollDuration: _scrollDuration,
            direction: _direction,
            scrollTo: _scrollTo,
          }
          parent.postMessage({ newStateData }, '*');
    }

    
    utilities.wordpress.child.handledLoadingOnParent = function (_isVisibled)  {
        const newStateData = {
            name: 'spinnerEvent',
            isVisibled: _isVisibled,
          }
          parent.postMessage({ newStateData }, '*');
    }
    
    utilities.wordpress.child.handledMessageOnParent = function (_XHR, _status, _title, _type, _options)  {
          var newStateData = {};
            newStateData = {
                name: 'displayMessageEvent',
                XHR: _XHR, 
                status : _status, 
                title: _title,
                type: _type,
                options: _options
                }
          
          parent.postMessage({ newStateData }, '*');
    }

    utilities.wordpress.child.redirectUrlInParent = function (_url)  {
        var newStateData = {};
          newStateData = {
            name: 'redirectUrlEvent',
            url: _url, 
        }
        parent.postMessage({ newStateData }, '*');
    }
    

    const resizeIframeFromParent = (_childHeight) => {
        const newStateData = {
          name: 'rizeParentHeightEvent',
          childHeight: _childHeight
        }
       
        parent.postMessage({ newStateData }, '*');
    }

    //Listener of event in the child to recevie messages or request from the parent frame
    utilities.wordpress.child.initChildEventListener = function (e) {
        var eventMethod = window.addEventListener
        ? "addEventListener"
        : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod === "attachEvent"
        ? "onmessage"
        : "message";
        eventer(messageEvent, function (e) {
            
        //Origin allows
        if (!(e.origin === 'https://ww2.example.ca' 
        || e.origin === 'https://localhost' 
        || e.origin === 'https://example.ca' 
        || e.origin === 'https://www.example.ca')) return;

        if(e.data && e.data.newStateData){
            if (e.data.newStateData.name === "sessionRestoreFromParentEvent" ) 
            {
                utilities.callbackTooltipOnRestoringSession ();
            }
        }
        });
    };

}(window.utilities = window.utilities || {}, jQuery));


