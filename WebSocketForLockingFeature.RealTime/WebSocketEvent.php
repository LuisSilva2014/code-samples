
<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use App\Repository\Models\Config;


class WebSocketEvent  implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $payload;
    public $given_channel;
    public $isLeaving;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($payload)
    {
        $this->payload = $payload;
        $this->given_channel = $payload['givenChannel'];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // Dynamic channel on the same hud shared server just to manage the dedicate app and in its proper environment, so we can avoid mess it out
        return new Channel('webSocketServer_'.$this->given_channel);
    }

    public function broadcastWith()
    {
        return [
             'broadcastedData' => $this->payload 
         ];
    }
    
}
