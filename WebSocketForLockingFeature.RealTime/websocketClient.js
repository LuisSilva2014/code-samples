
(function (webSocketClient, $, undefined) {

    webSocketClient.lockedMsg = '';
    webSocketClient.fallBackStatesList= ['unavailable', 'failed', 'disconnected', 'connecting'];
    webSocketClient.urlTakenBySocketId = null ;
    webSocketClient.userLocker = '';
    webSocketClient.userLockerId = '';
    webSocketClient.currentSocketId = '';
    webSocketClient.failed= false;
    webSocketClient.bodyReacted= false;
    webSocketClient.paramsInCurrentUrl= [];
    let goaldReactedOnContentLocked = false;

    function delayOnClosingBrowser(ms) {
        var start = +new Date;
        while ((+new Date - start) < ms);
    }

    webSocketClient.destroyModals = function () {
        var ob = $('.modal-backdrop, .modal');
        ob || ob.modal('hide');
        ob.remove();
        if ($('body').hasClass('modal-open')) {$('body').removeClass('modal-open')};
    }   

    webSocketClient.enableEventOnClosingTab = function () {

        window.addEventListener('beforeunload', (event) => {

            if (webSocketClient.isUserTheOwnerOfThisRoute !== undefined  && webSocketClient.isUserTheOwnerOfThisRoute(webSocketClient.currentSocketId , webSocketClient.getCurrentPath())) {
                event.preventDefault();
                window.removeEventListener('unload', null);
                
                //Note: the event behavior is slightly different in firefox.
                window.addEventListener('unload', function (e) {
                    
                    e.preventDefault();
                    var wsHost = 'https://' + Echo.options.wsHost;
                    if (wsHost !== undefined) {
                        
                        //Release the uri in the server
                        let dataSeed = {
                            currentRoute: webSocketClient.getCurrentPath(),
                            senderSocketId: webSocketClient.currentSocketId,
                            userId: current_user.id ,
                            params : webSocketClient.paramsInCurrentUrl,
                            userFullName: current_user.first_name + ' ' + current_user.last_name
                        };
                        navigator.sendBeacon(jsRoutes.forWebSocket.releaseUriApiV2, JSON.stringify(dataSeed));

                        delayOnClosingBrowser(5000);
                        // Now notify the unlock action to the other susbcribers
                        let nodeChannel = (appName || 'alternative') + '_' + (appNode || '') + '_' + (runEnv || '' ) + '_channel';
                        let dataSeed2 = {
                            action: 'unLocked',
                            senderSocketId: webSocketClient.currentSocketId,
                            givenChannel: nodeChannel,
                            currentRoute: webSocketClient.getCurrentPath(),
                        };
                        navigator.sendBeacon(wsHost + '/webSockets/set-broadcasting', JSON.stringify(dataSeed2));
                        delayOnClosingBrowser(3000);
                    }
                    event.returnValue = '';
                });
            }
        });
    }

    webSocketClient.getCurrentPath = function () {
        return window.location.host + window.location.pathname.replace(/[\\\#,+()$~.'":*<>{}]/g, '');
    }

    webSocketClient.checkUriBacked = function (_api, _dtoFromAdmin) {
        webSocketClient.bodyReacted = false;
        let state = Echo.connector.pusher.connection.state;
        
        if (webSocketClient.fallBackStatesList.includes(state) ||  Echo.socketId() == undefined) {
            //Wait for a few miliseconds || try until teh connection healed
            Echo.connect();
            setTimeout(function(){
                utilities.showLoading();
                webSocketClient.checkUriBacked(_api, _dtoFromAdmin);
            }, 800);
        }
        else{

            //First: Check if the resource was taken in anticipation, just in case of a fallback connection
            let newSocketId = Echo.socketId();

            if(parseFloat(webSocketClient.urlTakenBySocketId) === parseFloat(webSocketClient.currentSocketId) ){
                    //Refresh the urlTakenBySocketId
                    webSocketClient.urlTakenBySocketId = newSocketId;

                    $.ajax({
                        type: 'POST',
                        headers: getTokenInHeaderToProperRoutes(jsRoutes.forWebSocket.retakeByOnwerUriApi),
                        url: jsRoutes.forWebSocket.retakeByOnwerUriApi,
                        contentType: "application/json",
                        data: JSON.stringify({
                            currentRoute: webSocketClient.getCurrentPath(),
                            senderSocketId: webSocketClient.urlTakenBySocketId,
                            oldSocketId:  webSocketClient.currentSocketId,
                            userId: current_user.id, 
                            params : webSocketClient.paramsInCurrentUrl,
                            userFullName: current_user.first_name + ' ' + current_user.last_name
                        }),
                        success: function (data, status, error) {
                            
                            if (!data.success) {
                                utilities.errorHandlerResponse({ message: lang.t('ownershipNotExist' )}, null, '');
                            }
                            else{
                                isStillTheOwner = true;
                            
                                if(!webSocketClient.bodyReacted ){
                                    
                                    setAndGetSocketInfoInBack({
                                        _newSocketId : newSocketId,
                                        _api : _api,
                                        _dtoFromAdmin: _dtoFromAdmin,
                                        _isStillTheOwner : true,
                                        _bodyReacted : false
                                    });

                                    webSocketClient.bodyReacted  = true;// Avoinding twice ajax call
                                }
                            }
                        },
                        error: function (respose, status, error) {
                            utilities.errorHandlerResponse({ message: lang.t("generalError") + ' ...' }, null, '');
                        }
                    });
            }else{
                setAndGetSocketInfoInBack({
                    _newSocketId : newSocketId,
                    _api : _api,
                    _dtoFromAdmin: _dtoFromAdmin,
                    _isStillTheOwner : true,
                    _bodyReacted : false
                });
            }
        }
    };
    
    var getTokenInHeaderToProperRoutes = function (_api) {
        let _headers =  null;
        if(_api === jsRoutes.forWebSocket.getOrSaveUri){
            //Add the toke to the current wen route
            _headers =  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} ;
        }else{
            //TODO: provide the laravel passport token to cover the api request.. In the mean time is blanc
            _headers = {};
        }
        return _headers;
    }
    

    var setAndGetSocketInfoInBack = function (o) {
        // Assign the new socketId
        webSocketClient.currentSocketId = o._newSocketId;

        $.ajax({
            type: 'POST',
            url: o._api,
            headers: getTokenInHeaderToProperRoutes(o._api) ,
            contentType: "application/json",
            data: JSON.stringify({
                currentRoute: webSocketClient.getCurrentPath(),
                senderSocketId: webSocketClient.currentSocketId,
                userId: current_user.id ,
                params: webSocketClient.paramsInCurrentUrl,
                userFullName: current_user.first_name + ' ' + current_user.last_name
            }),
            success: function (data) {
                if(!o._bodyReacted){
                 
                    //Get status from server side
                    if(data._mode !== undefined && data._mode == 'checkSocketId'){
                        webSocketClient.urlTakenBySocketId =  data.urlTakenBySocketId == undefined ?  webSocketClient.urlTakenBySocketId : data.urlTakenBySocketId;
                    }else if(!o._dtoFromAdmin){
                        webSocketClient.urlTakenBySocketId =  data.urlTakenBySocketId;
                        webSocketClient.userLocker =  o._dtoFromAdmin ? o._dtoFromAdmin.first_name : data.userLocker;
                        webSocketClient.userLockerId = data.userLockerId ;
                    }


                    //Now, lets check the status of the current, and locked down if applies
                    if((parseFloat(webSocketClient.urlTakenBySocketId) !== parseFloat(webSocketClient.currentSocketId) && !o._isStillTheOwner )  || 
                            data.lockedByRelatedUrl
                    ){
                            webSocketClient.locked(
                                { 
                                    first_name : o._dtoFromAdmin ?  o._dtoFromAdmin.first_name : webSocketClient.userLocker, 
                                    last_name : o._dtoFromAdmin ?  o._dtoFromAdmin.last_name : '',
                                    url_locked : data.url_locked,
                                }
                            );
                    }
                    o._bodyReacted = true; //Avoinding twice ajax call 
                }
            },
            error: function (respose, status, error) {
                utilities.hideLoading();
                console.log('webSocketClient.getOrSaveUri ' + '>>> Error ' + JSON.stringify(respose));
                // utilities.errorHandlerResponse({ message: lang.t("failedCheckingUrl") + ' ...' }, null, '');
            }
        });
    }

    webSocketClient.removeAllActiveResourcesByDate = function (_untilDateTime) {
        if(_untilDateTime == ""){
            utilities.errorHandlerResponse({ message: lang.t("Please enter date") }, null, '');
            return false;
        }
        //Example of uri to set in the cron to remove theinacctive until last week
  

        utilities.showLoading();
        $.ajax({
            type: 'POST',   
            headers: { } , 
            url: jsRoutes.forWebSocket.removeAllActiveResourcesByDate +  '/'+ _untilDateTime,
            contentType: "application/json",
            success: function (data) {
                if (!data.success) {
                    utilities.errorHandlerResponse({ message: lang.t("noExecutedMsg") }, null, '');
                }else{
                    utilities.displayToasterMessage({ message: lang.t("executedMsg") }, 'Total Removed: ' + data.totalRemoved, 'success', {
                        timeOut: 2000,
                        fadeOut: 2000,
                    });
                    //Update the text
                    $('#totalRecords').html(data.totalRecords);
                }
                utilities.hideLoading();
            },
            error: function (respose, status, error) {
                utilities.hideLoading();
                utilities.errorHandlerResponse({ message: lang.t("generalError") + ' ...' }, null, '');
            }
        });
    }

    webSocketClient.getActiveLiveRecords = function () {
        $.ajax({
            type: 'GET',
            headers: { } ,
            url: jsRoutes.forWebSocket.getActiveLiveRecords,
            contentType: "application/json",
            success: function (data) {
                if (!data.success) {
                    utilities.errorHandlerResponse({ message: 'Error calling on getActiveLiveRecords  '}, null, '');
                }else{
                    $('#lockTable').html('');
                    if(data.result !== undefined &&  data.result.length > 0){

                        data.result.forEach(element => {
                            var htmlTRow =   "<tr id= " + element.id+ "  class='row'>" +
                            "<td class='col-md-1'>" + element.id+ "</td>" +
                            "<td class='col-md-2'>" + element.url_locked + "</td>" +
                            "<td class='col-md-1'>" + element.user_by_url + "</td>" +
                            "<td class='col-md-2'>" + element.echo_socket_id + "</td>" +
                            "<td class='col-md-2'>" + element.created_at + "</td>" +
                            "<td class='col-md-1'>" + element.updated_at + "</td>" +
                            "<td class='col-md-1'>" + element.created_by + "</td>" +
                            "<td class='col-md-1'>" + element.updated_by + "</td>" +
                            "<td class='col-md-1'> <a class='btn btn-danger' href='#' onclick='webSocketClient.manualDesactivationAndNotification("+ JSON.stringify(element)+")'><span class='glyphicon glyphicon-trash'></span>Remove</a></td>"+
                            "</tr>";
                            $('#lockTable').append(htmlTRow)
                        });
                    }else{
                        $('#lockTable').append( "<tr><p>'There is no active records')}}</tr>")
                    }

                    utilities.hideLoading();
                } 
            },
            error: function (respose, status, error) {
                utilities.hideLoading();
                clearInterval(getActiveLiveRecordsInterval);
                utilities.errorHandlerResponse({ message: lang.t("ajaxError") + ' ...' }, null, '');
            }
        });
    }


    webSocketClient.getResourcesFromConfiguration = function (e) {
        
        $.ajax({
            type: 'GET',
            url: jsRoutes.forWebSocket.getResourcesFromConfiguration,
            // async: true,
            contentType: "application/json",
            success: function (data) {

                //Check if the current url is not on the list of lockout urls
                if (typeof data.lockoutUrls !== 'undefined' && !webSocketClient.bodyReacted) {
                
                    
                    let currentPathName =  $.grep(window.location.pathname.split('/'), function(item) { 
                        if(typeof item === 'string' && item !== ''  &&  item.toLowerCase() !== 'public' )
                        {
                            //Filter usign regex express of special characters, except / and - symbols
                            return item.replace(/[\\\#,+()$~.'":*<>{}]/g, '');
                        };
                    });
                    
                    let currentRouteBy1Position = '/' + currentPathName[0];
                    let currentRouteBy2Positions = currentRouteBy1Position  +'/' + currentPathName[1];
                    let currentRouteBy3Positions = currentRouteBy2Positions  +'/' + currentPathName[3];
                    let currentRouteBy4Positions = currentRouteBy3Positions  +'/' + currentPathName[4];
                    let currentRouteBy5Positions = currentRouteBy4Positions  +'/' + currentPathName[5];

                    let ignoredActionList = ['new'];
                    let routeShouldBeVerified = false;

                    for (let index = 0; index < data.lockoutUrls.length; index++) {
                        const patternSetInUri = data.lockoutUrls[index];

                        //Only supporting up to 5 constanst positions
                        let isRouteExistingBy1 = patternSetInUri.includes(currentRouteBy1Position);
                        let isRouteExistingBy2 = patternSetInUri.includes(currentRouteBy2Positions);
                        let isRouteExistingBy3 = patternSetInUri.includes(currentRouteBy3Positions);
                        let isRouteExistingBy4 = patternSetInUri.includes(currentRouteBy4Positions);
                        let isRouteExistingBy5 = patternSetInUri.includes(currentRouteBy5Positions);

                        if (isRouteExistingBy1 || isRouteExistingBy2 || isRouteExistingBy3 || isRouteExistingBy4 || isRouteExistingBy5  ) {
                        
                            // Exclude the empty slases from the given route
                            let patternExpectedRoute = $.grep(patternSetInUri.split('/'), function(item) { 
                                return (typeof item === 'string' && item !== '');
                            });

                            for (i = 0; i < patternExpectedRoute.length; i++) {
                                if (!ignoredActionList.includes(currentPathName[i])) {

                                    //Mapping/replacing to the expected pattern in the current path
                                    if (patternExpectedRoute[i] == '{string}') {
                                        //Exclude in the current params of the route this value
                                        if (currentPathName[i] != undefined && currentPathName[i] !== null){
                                            webSocketClient.paramsInCurrentUrl.push({ 'string' : currentPathName[i] } ) ;
                                            currentPathName[i] = '{string}';
                                        }
                                    }
                                    else if (patternExpectedRoute[i] == '{id}') {
                                        if (currentPathName[i] != undefined && currentPathName[i] !== null)
                                        {
                                            webSocketClient.paramsInCurrentUrl.push({ 'id' : currentPathName[i] } ) ;
                                            currentPathName[i] = '{id}';
                                        }
                                    }
                                }
                            }
                            
                            //Check if the pattern in routes matches 
                            if (patternExpectedRoute.join('/') === currentPathName.join('/')) {

                                if (parseInt(data.sockets_enabled) !== 1) {
                                    utilities.displayToasterMessage({ message: lang.t("notRealTimeData") }, lang.t("socketsDisabled"), 'warning', {
                                        timeOut: 6000,
                                        fadeOut: 6000,
                                    });
                                } else {
                                    //Init the socket listener
                                    webSocketClient.clientEventHandler();
                                    routeShouldBeVerified = true;
                                }
                                break; //Finish the for loop, the route was encountered and resolved
                            } 
                        }
                        //Reset to the current path for next iteration  
                        webSocketClient.paramsInCurrentUrl = [];
                        currentPathName = $.grep(window.location.pathname.split('/'), function(item) { 
                            if(typeof item === 'string' && item !== ''  &&  item.toLowerCase() !== 'public' )
                            {
                                //Filter usign regex express of special characters, except / and - symbols
                                return item.replace(/[\\\#,+()$~.'":*<>{}]/g, '');
                            };
                        });
                        
                    }

                    //Verify if it should be validated
                    if (routeShouldBeVerified) {
                        Echo.connector.connect();
                        //Check first if the status of the route taken
                        webSocketClient.checkUriBacked(jsRoutes.forWebSocket.getOrSaveUri);
                        //Second: Make available the listerner
                        webSocketClient.notifyToOtherSubscribers('checkNewSubscriber');
                    } else {
                        //Disconected the current subscriber to not emit any error in the console
                        Echo.connector.disconnect()
                    }
                    webSocketClient.bodyReacted = true;
                }
            },
            error: function (respose, status, error) {
                console.log('Get routes configuration ' + '>>>Error ' + JSON.stringify(respose));
                utilities.errorHandlerResponse({ message: respose.responseJSON ? respose.responseJSON : '' }, null, 'Get route from server error');
            }
        });
    };

    webSocketClient.manualDesactivationAndNotification = function (_dto) {
        //Change the active status of the given sockect id
        $.ajax({
            type: 'POST',
            url: jsRoutes.forWebSocket.releaseUriApiV2,
            contentType: "application/json",
            data: JSON.stringify({
                currentRoute: _dto.url_locked,
                senderSocketId: _dto.echo_socket_id,
                userId: current_user.id, //_dto.updated_userId ,
                userFullName: current_user.first_name + ' ' + current_user.last_name
            }),
            success: function (data) {
                utilities.hideLoading();
                if (!data.success) {
                    utilities.errorHandlerResponse({ message: lang.t("noExecutedMsg") }, null, '');
                }else{
                    utilities.displayToasterMessage({ message: lang.t("executedMsg") }, '', 'success', {
                        timeOut: 2000,
                        fadeOut: 2000,
                    });
                    //Remove from the table
                    $('#'+ _dto.id).remove();
                    //Now notify everyone that the resource have been desactivated to refresh ther page
                    let _adminDto =       {
                        currentRoute: _dto.url_locked,
                        first_name: current_user.first_name,
                        last_name:  current_user.last_name + ' [SysAdmin]'
                    }
                    webSocketClient.notifyToOtherSubscribers('sockedIdDisabled', null, _adminDto);
                }
            },
            error: function (respose, status, error) {
                utilities.hideLoading();
                utilities.errorHandlerResponse({ message: lang.t("generalError") + ' ...' }, null, '');
            }
        });
    }

    webSocketClient.notifyToOtherSubscribers = function (_action, skippValidation, _dtoFromAdmin) {
        let state = Echo.connector.pusher.connection.state;
        let wsHost = (Echo.options.encrypted ? 'https' : 'http') + '://' + Echo.options.wsHost;
        let nodeChannel = (appName || 'alternative') + '_' + (appNode || '') + '_' + (runEnv || '' ) + '_channel';

        if (webSocketClient.fallBackStatesList.includes(state) || skippValidation) {
            //Wait for a few miliseconds || try manual connection 
            Echo.connect();
            setTimeout(function(){
                utilities.showLoading();
                webSocketClient.notifyToOtherSubscribers(_action, skippValidation, _dtoFromAdmin);
            }, 1000);
        }
        else if (state === 'connected' && current_user !== undefined) {

            //Check if there is any noconnection modal as a residual in case of offiline connection escenario
            if ($('#bs-noconnection-modal').hasClass('in')) {
                // $('#bs-noconnection-modal').remove();
                webSocketClient.destroyModals();
            };
        
            let _cRoute =  webSocketClient.getCurrentPath();
            let _uFirstName =  current_user.first_name;
            let _uLastName =  current_user.last_name;
            if(_action == 'sockedIdDisabled')
            {
                _cRoute = _dtoFromAdmin.currentRoute;
                _uLastName = _dtoFromAdmin.last_name;
                _uFirstName = _dtoFromAdmin.first_name;
            }
        
            $.ajax({
                type: 'POST',
                url: wsHost + '/webSockets/set-broadcasting',
                contentType: "application/json",
                // beforeSend: function (xhr) {
                //     xhr.setRequestHeader('Authorization', 'Basic ' + btoa('myuser:mypswd'));
                // },
                data: JSON.stringify({
                    givenChannel: nodeChannel,
                    action: _action,
                    currentRoute: _cRoute,
                    senderSocketId: webSocketClient.currentSocketId,
                    first_name: _uFirstName,
                    last_name: _uLastName
                }),
                success: function (data) {
                    utilities.hideLoading();
                    if (!data) {
                        utilities.errorHandlerResponse({ message: 'Error setting broadcasting' }, null, '');
                    }
                },
                error: function (respose, status, error) {
                    utilities.hideLoading();
                    console.log('Echo.connector ' + '>>> Error setting broadcasting' + JSON.stringify(respose));
                    utilities.errorHandlerResponse({ message: lang.t("clientNoAbleToGetComex")  }, null, lang.t("noExecutedMsg") );
                }
            });
        }
    };
    
    webSocketClient.clientEventHandler = function () {
        try {
            //Listener
            let nodeChannel = (appName || 'alternative') + '_' + (appNode || '') + '_' + (runEnv || '' ) + '_channel';
            Echo.channel('webSocketServer_' + nodeChannel)
                .listen('WebSocketEvent', (incomingSubscriber) => {

                    let b = incomingSubscriber.broadcastedData;
                    if (!b) { utilities.errorHandlerResponse({ message: 'There was an error communicating with the server, some PROMIS functionality will be unavailable.<br><br>' }, null, 'WebSocket server error'); return false; }
                    //Avoid send messages to itself
                    if (parseFloat(b.senderSocketId) === parseFloat(webSocketClient.currentSocketId))  return;
                    switch (b.action) {
                        case 'checkNewSubscriber':
                                webSocketClient.checkUriBacked(jsRoutes.forWebSocket.apiCheckLastestActiveUriBySocketId, { first_name : b.first_name,  last_name : b.last_name});
                                //Allow only the socketId that have taken this uri
                                if (webSocketClient.isUserTheOwnerOfThisRoute(webSocketClient.currentSocketId, b.currentRoute )) {
                                        //Reject the incoming connection of the new subscriber
                                        webSocketClient.notifyToOtherSubscribers('locked');
                                        webSocketClient.destroyModals();
                                }else if (b.currentRoute === webSocketClient.getCurrentPath()) 
                                        webSocketClient.locked(b);
                            break;

                        case 'locked':
                                if(parseFloat(webSocketClient.urlTakenBySocketId) !== parseFloat(webSocketClient.currentSocketId) ){
                                    if (b.currentRoute === webSocketClient.getCurrentPath()) 
                                    webSocketClient.locked(b);
                                }
                            break;
                            
                        case 'unLocked':
                            if (b.currentRoute === webSocketClient.getCurrentPath()) {
                                if (!$('#bs-lockrefresh-modal').hasClass('in') ) {
                                    webSocketClient.destroyModals();
                                    main.createLockRefreshModal(lang.t("contentUnLockedTitle"), lang.t("resourceUnLocked"));
                                }
                            }
                            break;

                        case 'sockedIdDisabled':
                            //Check again the values in server, they might cahnge if an admin kiil the existing session
                            if (b.currentRoute === webSocketClient.getCurrentPath()) {
                                webSocketClient.notifyToOtherSubscribers('unLocked');
                                webSocketClient.checkUriBacked(jsRoutes.forWebSocket.apiCheckLastestActiveUriBySocketId, { first_name : b.first_name,  last_name : b.last_name});
                            }
                        break;
                                
                    }
                });
                

        } catch (error) {
            console.error('Error at: webSocketClient.clientEventHandler'  + JSON.stringify(error));
            if(typeof webSocketClient.noConnectionEstablished == "undefined" ) webSocketClient.noConnectionEstablished();
        }

        webSocketClient.locked = function (b) {
            
            if (!$('#bs-lock-modal').hasClass('in') && !goaldReactedOnContentLocked ) {
                // let u = incomingSubscriber.broadcastedData.userLocker
                let relatedUrl = webSocketClient.getCurrentPath();
                if(b.url_locked){
                    relatedUrl = location.protocol + '//' + b.url_locked;
                }
                let msg = lang.t("resourceOpenedInAnotherSession") +  '<br><br>' + relatedUrl;
                webSocketClient.lockedMsg =msg.replace('STATIC_FULLNAME', webSocketClient.userLocker == '' ? b.first_name + ' ' + b.last_name : webSocketClient.userLocker);

                if(typeof main.createLockModal == "undefined" || typeof toastr == "undefined"){
                    setTimeout(function(){
                        // goaldReactedOnContentLocked = false;
                        webSocketClient.locked(b);
                    }, 200);
                }else{
                    webSocketClient.destroyModals();
                    utilities.errorHandlerResponse({ message: lang.t("contentLockedTitle") + ' ...' }, null, '');
                    setTimeout(function(){
                            main.createLockModal(lang.t("contentLockedTitle"), webSocketClient.lockedMsg, 'bs-lock-modal');
                            $('.modal-title,.modal-body,.modal-footer').show();
                    }, 1500);
                    goaldReactedOnContentLocked = true;
                }
            }else goaldReactedOnContentLocked = false;
        }

        webSocketClient.noConnectionEstablished = function () {
            if (!$('#bs-noconnection-modal').hasClass('in')) {
                main.createLockModal(lang.t("connectionLostTitle"), lang.t("noConnectionEstablishedMsg"), 'bs-noconnection-modal');
            };
        }

        webSocketClient.connectionEntablishedOrRestored = function () {
            //Restore the modal status in case that there is a tabTaken
            webSocketClient.destroyModals();
            utilities.hideLoading();
            if (webSocketClient.isUserTheOwnerOfThisRoute(webSocketClient.currentSocketId, webSocketClient.getCurrentPath())) 
                main.createInfoModal(lang.t("connectionRestoredTitle"), lang.t("connectionRestoredMsg"));
            else 
                main.createLockModal(lang.t("contentLockedTitle"), webSocketClient.lockedMsg, 'bs-lock-modal');
    
            utilities.displayToasterMessage({ message: lang.t("connectionEstablishedMsg") }, '', 'success', {
                timeOut: 2000,
                fadeOut: 2000,
            });
        }

        webSocketClient.registerGraceEvent = function () {
            $('.graceBtnForNoAbleToRestored').click(function(){
                webSocketClient.destroyModals();
                utilities.displayToasterMessage({ message: lang.t("graceTimeMsg") }, 'Grace time', 'info', {
                    timeOut: 30000,
                    fadeOut: 30000,
                });
                setTimeout(function(){
                    utilities.showLoading();
                    window.location.reload();
                }, 31000);
            });
        }

        Echo.connector.pusher.connection.bind('failed', () => {
            utilities.hideLoading();
            //This event happens when the same client lose server connection multiple times, example the server is restarted 3 times in less than 5 min
            webSocketClient.failed= true;
            webSocketClient.destroyModals();
            main.createLockModal('Unrecoverable fallback', lang.t("noAbleToRestoreConnectionMsg"), 'bs-noAbleToRestored-modal');
            if(webSocketClient.isUserTheOwnerOfThisRoute(webSocketClient.currentSocketId)) {
                webSocketClient.registerGraceEvent();
            }

            //Report the issue 
            let c= 'The system detected that websockets connection on wss://'+ webSocketsServer.url  +':'+ webSocketsServer.port +' is not responding. Please proceed to refresh cache using the link (http://' + webSocketsServer.url+ '/artisan/refreshLaravelCache ) or reboot the entire server if it required<br><br> <b>Details<b>:<br> '+
                '<p><strong> Browser path URI: </strong>' + webSocketClient.getCurrentPath() + ' </p>' +
                '<p><strong> Environment: </strong>' + runEnv + ' </p>' +
                '<p><strong> Node: </strong> ' + appNode + ' </p>' +
                '<p><strong> CurrentUser: </strong> ' + (current_user?.first_name || 'NA') + ' ' + (current_user?.last_name || '') + ' </p>' ;
            let _content = {
                _subject:  '[FailState] Unable to reach Comex server',
                _mailMessage : c,
                _reason : 'WEBSOCKETS_SERVER_FAILURE',
                _details :  webSocketClient.getCurrentPath(),
            };
            
            utilities.sendEmail(_content);
        });

        Echo.connector.pusher.connection.bind('state_change', function (states) {
            if (
                states.previous === 'connected' && states.current === 'unavailable'
                || states.previous === 'initialized' && states.current === 'failed'
                || states.previous === 'connected' && states.current === 'failed'
                // || states.previous === 'connecting' && states.current === 'unavailable'
                // || states.previous === 'connected' && states.current === 'connecting'
                )
            {
                // console.log(JSON.stringify(states))
                webSocketClient.noConnectionEstablished();
            }
            else if ( states.previous === 'unavailable' && states.current === 'connected'
                    || states.previous === 'failed' && states.current === 'connected')
                    // || states.previous === 'connecting' && states.current === 'connected')
            {  
                    //Doble check in case there is an update from admin
                    webSocketClient.checkUriBacked(jsRoutes.forWebSocket.apiCheckLastestActiveUriBySocketId, null);
                    setTimeout(function(){
                        webSocketClient.connectionEntablishedOrRestored();
                    }, 3000);

            }
        });

        Echo.connector.pusher.connection.bind('error', function (err) {
            if(err.error.data.code == 1006){
                webSocketClient.destroyModals();
                webSocketClient.noConnectionEstablished();
                utilities.errorHandlerResponse({ message: '[Fallback] ' + lang.t("webSocketOffline") }, null, '');
            }
        });
    };

    webSocketClient.isUserTheOwnerOfThisRoute = function (currentSocketId, routeFromSender ) {
        if(routeFromSender){
            return  parseFloat(webSocketClient.urlTakenBySocketId) === parseFloat(currentSocketId) 
            && webSocketClient.getCurrentPath() === routeFromSender
        }else{
            return  parseFloat(webSocketClient.urlTakenBySocketId) === parseFloat(currentSocketId) 
        }
    }

    webSocketClient.init = function () {
        //Initialized the listener
        webSocketClient.enableEventOnClosingTab();
        webSocketClient.getResourcesFromConfiguration();
    }

}(window.webSocketClient = window.webSocketClient || {}, jQuery));
