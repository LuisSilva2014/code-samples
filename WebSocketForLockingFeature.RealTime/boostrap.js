// /**
//  * Echo exposes an expressive API for subscribing to channels and listening
//  * for events that are broadcast by Laravel. Echo and event broadcasting
//  * allows your team to easily build robust real-time web applications.
//  */
window.Vue = {};

import Echo from 'laravel-echo';
window.Pusher = require('pusher-js');

//Escaping main configuration
(function (webSocketsServer, $, undefined) {
    webSocketsServer.url =process.env.MIX_WEBSOCKETS_SERVER;
    webSocketsServer.port =process.env.MIX_WEBSOCKETS_PORT;
}(window.webSocketsServer = window.webSocketsServer || {}, jQuery));

//Starting pusher connection
try {
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: process.env.MIX_PUSHER_APP_KEY,
        cluster: process.env.MIX_PUSHER_APP_CLUSTER,
        wsHost: process.env.MIX_WEBSOCKETS_SERVER,
        wsPort : parseInt(process.env.MIX_WEBSOCKETS_PORT),
        wssPort: parseInt(process.env.MIX_WEBSOCKETS_PORT),
        forceTLS: true,
        enabledTransports: ['ws','wss'],
        encrypted: true,
        disableStats: true,
    
    });
    //Handling error when the server is not reachable for a weird reason
    window.Echo.connector.pusher.connection.bind( 'error', function( err ) {
        
        if( err.error.data.code === 4004 ) {
          log('Over limit!');
        }
    });
} catch (error) {
    console.info(error);
}

//Handling fallbacks when the server is unreachable
var fallbackCount = 0;
Pusher.log = (msg) => {
    // if(runEnv !== 'production') console.log(msg);
    if(msg.includes('unavailable'))
    {
        fallbackCount++;
    }
    //When the fallback reaches 1000 times, let's notify the webmaster. /
    //100 it is  enough number to measure when the connection was not able to restabished
    if(fallbackCount === 1000 ){
        let c= 'The system detected that websockets connection on wss://'+ webSocketsServer.url  +':'+ webSocketsServer.port +' is not responding. Please proceed to refresh cache using the link (http://' + webSocketsServer.url+ '/artisan/refreshLaravelCache ) or reboot the entire server if it required<br><br> <b>Details<b>:<br> '+
        '<p><strong> Browser path URI: </strong>' + webSocketClient.getCurrentPath() + ' </p>' +
        '<p><strong> Environment: </strong>' + runEnv + ' </p>' +
        '<p><strong> Node: </strong> ' + appNode + ' </p>' +
        '<p><strong> CurrentUser: </strong> ' + (current_user?.first_name || 'NA') + ' ' + (current_user?.last_name || '') + ' </p>' ;
        let _content = {
            _subject:  '[InitialState] Unable to reach Comex server',
            _mailMessage : c,
            _reason : 'WEBSOCKETS_SERVER_FAILURE',
            _details :  webSocketClient.getCurrentPath(),
        };
        utilities.sendEmail(_content);
    }
  };
